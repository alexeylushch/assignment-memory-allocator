CFLAGS = --std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC = gcc

BUILDDIR = build
SRCDIR = src
PROGAMDIR = $(BUILDDIR)/program


C_FILES = $(wildcard $(SRCDIR)/*.c)
O_FILES = $(patsubst $(SRCDIR)/%.c, $(BUILDDIR)/%.o, $(C_FILES))

PROJECTNAME = Allocator

all: build program $(O_FILES)
	$(CC) $(O_FILES) -o $(PROGAMDIR)/$(PROJECTNAME)
build:
	mkdir -p $(BUILDDIR)
program:
	mkdir -p $(PROGAMDIR)
$(BUILDDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -c $(CFLAGS) $< -o $@
clean:
	rm -rf $(BUILDDIR)
