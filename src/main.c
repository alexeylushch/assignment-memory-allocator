#include <stdio.h>
#include "mem.h"
#include "test.h"
#include "inttypes.h"


#define HEAP_SIZE 1500


void run_test() {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
}

int main(void) {

    __attribute__((unused)) void* heap = heap_init(HEAP_SIZE);
    run_test();
    return 0;
}


#undef HEAP_SIZE