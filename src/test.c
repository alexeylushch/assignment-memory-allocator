#define _DEFAULT_SOURCE
#include <inttypes.h>
#include <stdbool.h>

#include "test.h"
#include "mem.h"
#include "mem_internals.h"

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}


static void start_test(int8_t number) {
    printf("=============== Start Test # %" PRId8 " =================\n\n", number);
}
static void end_test(int8_t number) {
    printf("\n================= End Test # %" PRId8 " =================\n\n", number);
}

static bool check_capacity(struct block_header* header, uint64_t capacity) {
    return header->capacity.bytes == capacity;
}
static bool check_free(struct block_header* header, bool isFree) {
    return header->is_free == isFree;
}
static bool check_next(struct block_header* header, struct block_header* next) {
    return header->next == next;
}
static bool check_headers(struct block_header* first, struct block_header* second) {
    return first == second;
}

static bool show_check_capacity(struct block_header* header, uint64_t capacity) {

    bool result_check_capacity = check_capacity(header, capacity);

    if (result_check_capacity) {
        printf("Capacity check: PASSED.\n");
        return result_check_capacity;
    }
    printf("Capacity check: FAILED. (Excepted: %" PRId64 ". Result: %" PRId64")\n", capacity, header->capacity.bytes);
    return result_check_capacity;
}
static bool show_check_free(struct block_header* header, bool isFree) {

    bool result_check_free = check_free(header, isFree);

    if (result_check_free) {
        printf("Free check: PASSED.\n");
        return result_check_free;
    }
    printf("Free check: FAILED. (Excepted: %s. Result: %s)\n", isFree ? "true" : "false", result_check_free ? "true" : "false");
    return result_check_free;
}
static bool show_check_next(struct block_header* header, struct block_header* next) {
    bool result_check_next = check_next(header, next);

    if (result_check_next) {
        printf("Next address check: PASSED.\n");
        return result_check_next;
    }
    printf("Next address check: FAILED. (Excepted: %10p. Result: %10p)\n", header->contents, next->contents);
    return result_check_next;
}
static bool show_check_headers(struct block_header* first, struct block_header* second) {
    bool result_check_headers = check_headers(first, second);

    if (result_check_headers) {
        printf("Address check: PASSED.\n");
        return result_check_headers;
    }
    printf("Address check: FAILED. (First: %10p. Second: %10p)\n", first->contents, second->contents);
    return result_check_headers;
}

static void show_test_result(int8_t test_number , bool result) {
    if (result) {
        printf("\nTest %" PRId8 " passed!\n", test_number );
    }
    else { printf("\nTest %" PRId8 " failed!\n", test_number );}
}

void test_1(void) {

    int64_t test_number = 1;
    int64_t malloc_size = 50;

    start_test(test_number);

    void* obj_first = _malloc(malloc_size);
    struct block_header* header_obj_first = block_get_header(obj_first);

    bool test_result = (show_check_capacity(header_obj_first, malloc_size) && 
                        show_check_free(header_obj_first, false));

    show_test_result(test_number, test_result);



    _free(obj_first);
    end_test(test_number);    
}

void test_2() {

    int64_t test_number = 2;
    int64_t malloc_size = 150;

    start_test(test_number);

    void* obj_first = _malloc(malloc_size);
    void* obj_second = _malloc(malloc_size);
    void* obj_third = _malloc(malloc_size);


    struct block_header* header_obj_first = block_get_header(obj_first);
    struct block_header* header_obj_second = block_get_header(obj_second);
    struct block_header* header_obj_third = block_get_header(obj_third);




    _free(obj_first);


    bool test_result = (    show_check_capacity(header_obj_first, malloc_size) && 
                            show_check_free(header_obj_first, true) &&
                            show_check_capacity(header_obj_second, malloc_size) && 
                            show_check_free(header_obj_second, false) &&
                            show_check_capacity(header_obj_third, malloc_size) && 
                            show_check_free(header_obj_third, false)
                        );

    show_test_result(test_number, test_result);

    _free(obj_second);
    _free(obj_third);
    end_test(test_number);
}


void test_3(void) {


    int64_t test_number = 3;
    int64_t malloc_size = 150;

    start_test(test_number);

    void* obj_first = _malloc(malloc_size);
    void* obj_second = _malloc(malloc_size);
    void* obj_third = _malloc(malloc_size);


    struct block_header* header_obj_first = block_get_header(obj_first);
    struct block_header* header_obj_second = block_get_header(obj_second);
    struct block_header* header_obj_third = block_get_header(obj_third);




    _free(obj_first);
    _free(obj_third);


    bool test_result = (    header_obj_first != NULL &&
                            show_check_capacity(header_obj_first, malloc_size) && 
                            show_check_free(header_obj_first, true) &&
                            show_check_capacity(header_obj_second, malloc_size) && 
                            show_check_free(header_obj_second, false) &&
                            show_check_next(header_obj_third, NULL)
                        );

    show_test_result(test_number, test_result);

    _free(obj_second);
    end_test(test_number);
}


void test_4(void) {
    int64_t test_number = 4;
    int64_t malloc_size = 16000;

    start_test(test_number);

    void* obj_first = _malloc(malloc_size);
    struct block_header* header_obj_first = block_get_header(obj_first);

    bool test_result = (show_check_next(header_obj_first, NULL));

    show_test_result(test_number, test_result);



    end_test(test_number);    
}


void test_5(void) {

    int64_t test_number = 5;
    int64_t malloc_size = 100;

    start_test(test_number);

    void* obj_first = _malloc(malloc_size);
    struct block_header* header_obj_first = block_get_header(obj_first);

    map_pages(header_obj_first->contents + header_obj_first->next->capacity.bytes, REGION_MIN_SIZE, 0);

    void* obj_second = _malloc(1500);
    struct block_header* header_obj_second= block_get_header(obj_second);

    bool test_result = (    
                            show_check_headers(header_obj_first->next->next, header_obj_second)
                        );

    show_test_result(test_number, test_result);

    end_test(test_number);  
}

